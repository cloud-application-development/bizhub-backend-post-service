var admin = require('firebase-admin');
var {getAuth} = require('firebase-admin/auth');
var serviceAccount = require('../service_account_keyfile.json');

var app = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const verifyAndDecodeToken = async (idToken) => getAuth(app).verifyIdToken(idToken)
    .then((decodeToken) => {
        return decodeToken
    }).catch((error) => {
            console.log("Authentication failed!" + error.message)
            return false
        }
    );

const verifyClusterAccess = (tenantId) => {
    let clusterOwner = process.env.CLUSTER_OWNER;
     return clusterOwner === 'bizhub' ? true: tenantId === clusterOwner;
}


module.exports = {
    verifyAndDecodeToken,
    verifyClusterAccess,
}