const {app} = require('../service/authenticator')
const {Storage} = require('@google-cloud/storage');
const {v4: uuid} = require('uuid');
const stream = require('stream');
var serviceAccount = require('../service_account_keyfile.json');

const storage = new Storage({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});

const defaultBucket = `${serviceAccount.project_id}.appspot.com`;
const folderPrefix = `tenants/`

const uploadImageToBucket = (fireStoreDocumentId, rawImage, tenantId) => {
  return new Promise((resolve, reject) => {
      const itemUUID = uuid()
      const image = rawImage
      let pictureURL = ''

      const mimeType = image.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/)[1]

      const base64EncodedImageString = image.replace(/^data:image\/\w+;base64,/, '')

      const imageBuffer = Buffer.from(base64EncodedImageString, 'base64');

      const bufferStream = new stream.PassThrough();
      bufferStream.end(imageBuffer);

      const bucket = storage.bucket(defaultBucket)

      const file = bucket.file(`${folderPrefix}${tenantId}/${fireStoreDocumentId}`)

      bufferStream.pipe(file.createWriteStream({
          metadata: {
            metadata: {
              firebaseStorageDownloadTokens: itemUUID,
            },
          contentType: mimeType,
          },
          public: false,
          validation: "md5"
      }))
          .on('error', (err) => {
          console.log('error from image upload', err);
          })
          .on('finish', () => {
            resolve("https://firebasestorage.googleapis.com/v0/b/" + bucket.name + "/o/" + encodeURIComponent('tenants/' + tenantId + '/' + fireStoreDocumentId) + "?alt=media&token=" + itemUUID)
         });
  });
}

module.exports = {
  uploadImageToBucket
}