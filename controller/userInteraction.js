const serviceAccount = require('../service_account_keyfile.json');
const Firestore = require("@google-cloud/firestore");
const METADATA_COLLECTION = 'metadata';
const POST_INTERACTION_DOCUMENT = 'post_interactions';
const db = new Firestore({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});


const getInteractionStats = async (req, res) => {

    db.collection(METADATA_COLLECTION).doc(POST_INTERACTION_DOCUMENT).get().then((userInteractions) => {
        res.json(userInteractions.data());
    }).catch((error) => {
        res.status(503).send('Service Unavailable')
        console.log("Error while post interaction stats: " + error.message)
    });
}

module.exports = {
    getInteractionStats,
}