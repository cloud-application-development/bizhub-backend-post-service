const {verifyAndDecodeToken, verifyClusterAccess} = require('../service/authenticator');
const serviceAccount = require('../service_account_keyfile.json');
const Firestore = require("@google-cloud/firestore");
const ROOT_COLLECTION = 'tenants';
const USERS_COLLECTION = 'users';
const POSTS_COLLECTION = 'posts';
const COMMENTS_COLLECTION = 'comments';
const {v4: uuid} = require('uuid');
const {raw} = require("body-parser");
const db = new Firestore({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});


const newComment = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const userId = token.uid

    const postContent = req.body

    const commentId = uuid()
    const postId = req.params.postId
    const {comment} = postContent
    const {photoURL} = postContent
    const {displayName} = postContent

    if (!comment) { //if comment is empty
        res.status(400).send('Bad request')
        return;
    }

    const commentObjectData = {
        commentId: commentId,
        authorId: userId,
        comment: comment,
        photoURL: photoURL,
        displayName: displayName,
        timeStamp: Date.now()
    }

    const postRef = db.collection(ROOT_COLLECTION).doc(tenantId).collection(POSTS_COLLECTION).doc(postId);
    const post = await postRef.get()
    if (!post.exists) { //if postId doesnt exist in tenant
        res.status(400).send('Bad request')
        return;
    }

    postRef.collection(COMMENTS_COLLECTION).doc(commentId).set(commentObjectData).then(() => {
        res.status(200).json(commentObjectData)
    }).catch((error) => {
        res.status(503).send('Out of Resources')
        console.log("failed to write comment: " + error.message)
    })

}

const getComments = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const postId = req.params.postId

    if (!postId) { //ifpostId is empty
        res.status(400).send('Bad request')
        return;
    }

    const comments = [];
    const enrichedComments = [];

    await db.collection(ROOT_COLLECTION).doc(tenantId).collection(POSTS_COLLECTION).doc(postId).collection(COMMENTS_COLLECTION).orderBy('timeStamp').get().then((querySnapshot) => {
        querySnapshot.forEach(snapshot => {
            let data = snapshot.data();

            //TODO find solution for userdata
            //const user = await getUserFromFirestore(tenant, userId);
            comments.push({
                authorId: data.authorId,
                comment: data.comment,
                commentId: data.commentId,
                timeStamp: data.timeStamp,
                displayName: data.displayName,
                photoURL: data.photoURL,
            })
        })
        res.json(comments);
    }).catch((error) => {
        res.status(503).send('Service Unavailable')
        console.log("Error while getting comments: " + error.message)
    })

}


const deleteComment = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const userId = token.uid


    const postId = req.params.postId
    const commentId = req.params.commentId

    const postRef = db.collection(ROOT_COLLECTION).doc(tenantId).collection(POSTS_COLLECTION).doc(postId);
    const post = await postRef.get();

    if (!post.exists) {
        res.status(400).send('Bad Request post');
        return;
    }

    const commentRef = postRef.collection(COMMENTS_COLLECTION).doc(commentId);
    const comment = await commentRef.get()

    if (!comment.exists) {
        res.status(400).send('Bad Request');
        return;
    }

    const postAuthor = post.userId
    const commentAuthor = comment.authorId
    const isAdmin = await userIsTenantAdmin(tenantId, userId);

    if (!assessDeleteAllowed(userId, postAuthor, commentAuthor, isAdmin)) {
        res.status(403).send('Unauthorized');
        return;
    }

    commentRef.delete();
    res.status(200).send('Success');
}


function assessDeleteAllowed(requestingUser, postAuthor, commentAuthor, isAdmin) {
    return (requestingUser === postAuthor || requestingUser === commentAuthor || isAdmin)
}

async function userIsTenantAdmin(tenant, userId) {
    const user = await getUserFromFirestore(tenant, userId);
    if (user) { //if user exists
        return (user.admin || user.admin === 'true') //make sure booleans and strings are taken into account
    }
    return false;
}

async function getUserFromFirestore(tenant, userId) {
    let userData = ''
    await db.collection(ROOT_COLLECTION).doc(tenant).collection(USERS_COLLECTION).doc(userId).get().then((querySnapshot) => {
        if (!querySnapshot.empty) {
            userData = querySnapshot.data()
        } else {
            return false; //user not found
        }
    })
    return userData;
}

module.exports = {
    newComment,
    getComments,
    deleteComment,
}