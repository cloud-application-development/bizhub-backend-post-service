const {verifyAndDecodeToken, verifyClusterAccess} = require('../service/authenticator');
const serviceAccount = require('../service_account_keyfile.json');
const Firestore = require("@google-cloud/firestore");
const ROOT_COLLECTION = 'tenants';
const POSTS_COLLECTION = 'posts';
const LIKES_COLLECTION = 'likes';
const {v4: uuid} = require('uuid');
const db = new Firestore({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});


const newLike = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const userId = token.uid
    const likeId = userId // likeId is set to userid intentionally

    const postId = req.params.postId
    const postContent = req.body
    const {displayName} = postContent
    const {photoURL} = postContent

    const likeObjectData = {
        likerId: userId,
        displayName: displayName,
        photoURL: photoURL,
        timestamp: Date.now()
    }

    const postRef = db.collection(ROOT_COLLECTION).doc(tenantId).collection(POSTS_COLLECTION).doc(postId);
    const post = await postRef.get()
    if (!post.exists) { //if postId doesnt exist in tenant
        res.status(400).send('Bad request')
        return;
    }

    postRef.collection(LIKES_COLLECTION).doc(likeId).set(likeObjectData).then(() => {
        res.status(201).json(likeObjectData)
    }).catch((error) => {
        res.status(503).send('Out of Resources')
        console.log("failed to add like: " + error.message)
    })

}

const getLikes = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const postId = req.params.postId

    if (!postId) { //if postId is empty
        res.status(400).send('Bad request')
        return;
    }

    const likes = [];

    await db.collection(ROOT_COLLECTION).doc(tenantId).collection(POSTS_COLLECTION).doc(postId).collection(LIKES_COLLECTION).orderBy('timestamp').get().then((querySnapshot) => {
        querySnapshot.forEach(snapshot => {
            let data = snapshot.data();

            likes.push({
                likerId: data.likerId,
                displayName: data.displayName,
                photoURL: data.photoURL,
                timestamp: data.timestamp,
            })
        })
        res.json(likes);
    }).catch((error) => {
        res.status(503).send('Service Unavailable')
        console.log("Error while getting comments: " + error.message)
    })

}


const deleteLike = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const userId = token.uid
    const postId = req.params.postId
    const likeId = req.params.likeId

    if (userId !== likeId) { //only user can delete own like
        res.status(403).send('Forbidden')
        return;
    }

    const postRef = db.collection(ROOT_COLLECTION).doc(tenantId).collection(POSTS_COLLECTION).doc(postId);
    const post = await postRef.get();

    if (!post.exists) {
        res.status(400).send('Bad Request post');
        return;
    }

    const likeRef = postRef.collection(LIKES_COLLECTION).doc(likeId);
    const comment = await likeRef.get()

    if (!comment.exists) {
        res.status(400).send('Bad Request');
        return;
    }

    likeRef.delete();
    res.status(200).send('Success');
}


module.exports = {
    newLike,
    getLikes,
    deleteLike,
}