const {verifyAndDecodeToken, verifyClusterAccess} = require('../service/authenticator');
const {uploadImageToBucket} = require('../service/upload')
const Firestore = require('@google-cloud/firestore');
const {v4: uuid} = require('uuid');
const ROOT_COLLECTION = 'tenants';
const POST_COLLECTION = 'posts';
const COMMENTS_COLLECTION = 'comments';
const USER_COLLECTION = 'users';
const LIKES_COLLECTION = 'likes';
const serviceAccount = require('../service_account_keyfile.json');

const db = new Firestore({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});

const newPost = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant
    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const postId = uuid()

    postContent = sanatizePostRequestPayload(req)

    postContent.postId = postId
    postContent.tenantId = tenantId
    postContent.timeStamp = Date.now()

    db.collection(ROOT_COLLECTION).doc(tenantId).collection(POST_COLLECTION).doc(postId).set(postContent).then(() => {
        if (postContent.rawImage) {
            uploadImageToBucket(postContent.postId, postContent.rawImage, tenantId).then((firebaseImageUri) => {
                db.collection(ROOT_COLLECTION).doc(tenantId).collection(POST_COLLECTION).doc(postId).set(
                    {
                        firebaseImageUri: firebaseImageUri,
                        rawImage: Firestore.FieldValue.delete()
                    },
                    {merge: true}
                ).then((response) => {
                    db.collection(ROOT_COLLECTION).doc(tenantId).collection(POST_COLLECTION).doc(postId).get().then((response) => {
                        res.status(201).json(response.data());
                    })
                });
            })
        } else {
            res.status(201).json(postContent);
        }
    }).catch(() => {
        res.status(500).send('Could not persist post')
    })
};


const getPosts = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const posts = [];

    let dbRef;

    //default case
    dbRef = db.collection(ROOT_COLLECTION).doc(tenantId).collection(POST_COLLECTION).where('groupId', '==', null).orderBy('timeStamp', 'desc')

    if(req.params.groupId) {
        dbRef = db.collection(ROOT_COLLECTION).doc(tenantId).collection(POST_COLLECTION).where('groupId', '==', req.params.groupId).orderBy('timeStamp', 'desc')
    }

    const postLimit =  parseInt(req.query.postLimit);
    const offset = parseInt(req.query.offset);
    console.log(req.query.postLimit, offset)


    if(postLimit && offset !== undefined && postLimit >= 1){
        console.log(req.query.postLimit, offset)
        dbRef = dbRef.limit(postLimit).offset(offset)
    }

    dbRef.get().then( async (querySnapshot) => {

        for (const document of querySnapshot.docs){

            const comments = []
            await document.ref.collection(COMMENTS_COLLECTION).get().then((querySnapshot) => {
                querySnapshot.forEach((commentDocument) => {
                    comments.push(commentDocument.data())
                })
            });

            const likes = []
            await document.ref.collection(LIKES_COLLECTION).get().then((querySnapshot) => {
                querySnapshot.forEach((likeDocument) => {
                    likes.push(likeDocument.data())
                })
            });

            let data = document.data();

            data.comments = comments ? comments : undefined;
            data.likes = likes ? likes : undefined;

            posts.push(JSON.parse(JSON.stringify(data)))
        }

        res.json(posts);
    });
}

const deletePost = async (req, res) => {

    const token = await verifyAndDecodeToken(req.header('token'));
    const tenantId = token.firebase.tenant;

    let clusterAccess = verifyClusterAccess(tenantId);

    if (!token || !clusterAccess) { //if token could not be verified
        res.status(403).send('Forbidden')
        return
    }

    const userId = token.uid
    let postId = req.params.postId
    console.log("postId " + postId)

    const postRef = db.collection(ROOT_COLLECTION).doc(tenantId).collection(POST_COLLECTION).doc(postId);
    const post = await postRef.get();

    if (!post.exists) {
        res.status(451).send('Bad Request post ');
        return;
    }

    const postAuthor = post.userId
    const isAdmin = await userIsTenantAdmin(tenantId, userId);

    if(userId !== postAuthor && !isAdmin) { //requesting user is not author and not admin
        res.status(403).send('Unauthorized');
        return;
    }

    db.collection(ROOT_COLLECTION).doc(tenantId).collection(POST_COLLECTION).doc(postId).delete();
    res.status(200).send('Success');
}


async function userIsTenantAdmin(tenant, userId) {
    const user = await getUserFromFirestore(tenant, userId);
    if (user) { //if user exists
        return (user.admin || user.admin === 'true') //make sure booleans and strings are taken into account
    }
    return false;
}

async function getUserFromFirestore(tenant, userId) {
    let userData = ''
    await db.collection(ROOT_COLLECTION).doc(tenant).collection(USER_COLLECTION).doc(userId).get().then((querySnapshot) => {
        if (!querySnapshot.empty) {
            userData = querySnapshot.data()
        } else {
            return false; //user not found
        }
    })
    return userData;
}

const sanatizePostRequestPayload = (req) => {
    return JSON.parse(JSON.stringify({
        messageAsMarkdown: (req.body.messageAsMarkdown === null || req.body.messageAsMarkdown === undefined) ? undefined : req.body.messageAsMarkdown,
        messageAsHtml: (req.body.messageAsHtml === null || req.body.messageAsHtml === undefined) ? undefined : req.body.messageAsHtml,
        author: (req.body.author === null || req.body.author === undefined) ? undefined : req.body.author,
        rawImage: (req.body.rawImage === null || req.body.rawImage === undefined) ? undefined : req.body.rawImage,
        groupId: (req.params.groupId === null || req.params.groupId === undefined) ? null : req.params.groupId,
    }));
}

module.exports = {
    newPost,
    getPosts,
    deletePost,
}