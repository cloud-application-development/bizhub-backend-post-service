FROM node:17-alpine3.12
WORKDIR /app
ADD package*.json ./
RUN npm install
ADD index.js ./
ADD controller ./controller
ADD service ./service
ADD routes ./routes
CMD [ "node", "index.js"]