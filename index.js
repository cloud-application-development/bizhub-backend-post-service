const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const port = 8085

var postRoutes = require('./routes/post')

app.use(cors('*'))
app.use(bodyParser.json({limit: '5mb'}))
app.use(express.static('public'));

app.use('/api', postRoutes)

app.get('/', (req, res) => {
  res.send('Hello World!') //health readyness check
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})