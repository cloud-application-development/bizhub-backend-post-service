const express = require('express')
const postController = require('../controller/post')
const commentController = require('../controller/comment')
const likeController = require('../controller/like')
const userInterActionController = require('../controller/userInteraction')

var router = express.Router()

//ROUTES FOR GENERAL POSTS
router.post('/posts', postController.newPost);
router.get('/posts', postController.getPosts);
router.delete('/posts/:postId', postController.deletePost);

//ROUTES FOR COMMENTS
router.post('/posts/:postId/comments', commentController.newComment);
router.get('/posts/:postId/comments', commentController.getComments);
router.delete('/posts/:postId/comments/:commentId', commentController.deleteComment);

//ROUTES FOR LIKES
router.post('/posts/:postId/likes', likeController.newLike);
router.get('/posts/:postId/likes', likeController.getLikes);
router.delete('/posts/:postId/likes/:likeId', likeController.deleteLike);

//ROUTES FOR GROUP POSTS
router.post('/posts/groups/:groupId', postController.newPost)
router.get('/posts/groups/:groupId', postController.getPosts)
router.delete('/posts/groups/:groupId/:postId', postController.deletePost)

//ROUTES FOR OPEN INFORMATION
router.get('/posts/user-interactions', userInterActionController.getInteractionStats);

module.exports = router

